# gnome-shell-extension-arch-update

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Update indicator for Arch Linux and GNOME Shell

https://github.com/RaphaelRochet/arch-update

## Getting started

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/extensions/gnome-shell-extension-arch-update.git
```
